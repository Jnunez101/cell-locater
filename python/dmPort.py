import serial
from binascii import hexlify
import threading

#Check out mobile insight!

class dmPortReader:
    def __init__(self, path):
        self.serialPort = None
        self.createPort()
        self.running = True
        self.bufferLock = threading.Lock()
        self.readThread = threading.Thread(target=self.obtainData)

    def closePort(self):
        #close port
        self.serialPort.write(hexlify("7300000000000000da817e"))
        self.serialPort.close()

    def createPort(self, path):
        self.serialPort = serial.Serial(path, baudrate=115200, rtscts=True, dsrdtr=True)

        #strings obtained from mobile insight here! 
        # https://github.com/mobile-insight/mobileinsight-core

        # Disable all DM port output 
        self.serialPort.write(hexlify("7300000000000000da817e"))
        #enable the fancy 
        self.serialPort.write(hexlify("73000000030000000b0000000010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000080000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002b397e"))

    def obtainData(self):
        #read from the dm port! 
        #Make this safe i guess
        while self.running:
            output = self.serialPort.read(69)
            self.bufferLock.acquire()
            self.buffer += output
            self.bufferLock.release()

    def unescape(self,frame):
        #There are some characters to clean
        esc = False
        outputFrame = b""
        counter = 0
        while True:
            if counter == len(frame):
                break
            if esc:
                outputFrame += frame[counter] ^ 0x20;
                esc = False
                counter += 1
            elif frame[counter] == b"\x7d":
                esc = True
            else:
                outputFrame += frame[counter]
                counter += 1
        return outputFrame
    
    #Compare taking frame by frame like this or the entire buffer at a time
    def obtainFrame(self):
        #This function grabs the next avaliable frame (data segment) from the buffer!
        #frames are seperated by the hex 7E
        self.bufferLock.acquire()
        delim = self.buffer.find(b"\x7e")
        if delim == -1:
            #no frame found
            return None
        frame = self.buffer[:delim]
        self.buffer = self.buffer[delim:]
        self.bufferLock.release()
        return frame