




from asyncio import DatagramProtocol
import dmPort

def main():
    try:
        #create at port and dm port for the given modem
        dmPort = dmPort.dmPortReader("/dev/ttyUSB0")

        #obtain frame and process it in the parser!
        frame = dmPort.obtainData()

        print(frame)
        #parse the frame and get data from it!
        #Returned data structure is 
        #   dict: {pci: {beam: signal strength}}


    except:
        #close the dm port
        dmPort.closePort()
    


if __name__ == "__main__":
    main()